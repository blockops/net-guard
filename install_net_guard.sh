#!/bin/bash

# install packages
sudo date
sudo apt-get update && sudo apt-get install -y curl arp-scan git
# retrieve configuration file and main script
sudo curl -o /etc/telegram-notify.conf https://raw.githubusercontent.com/NicolasBernaerts/debian-scripts/master/telegram/telegram-notify.conf
sudo curl -o /usr/local/sbin/telegram-notify https://raw.githubusercontent.com/NicolasBernaerts/debian-scripts/master/telegram/telegram-notify
sudo curl -o /usr/local/sbin/net_guard.sh https://gitlab.com/blockops/net-guard/-/raw/master/net_guard.sh
sudo chmod +x /usr/local/sbin/telegram-notify
sudo chmod +x /usr/local/sbin/net_guard.sh
if [[ ! -f /etc/net_guard.conf ]]; then
  sudo curl -o /etc/net_guard.conf https://gitlab.com/blockops/net-guard/-/raw/master/net_guard.conf
fi 
sudo chmod 600 /etc/net_guard.conf
echo "Please update the settings /etc/net_guard.conf"
