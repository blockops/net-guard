<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Net Guard](#net-guard)
  - [OS compability](#os-compability)
  - [Features](#features)
  - [Install](#install)
    - [Linux and WSL Package Installation](#linux-and-wsl-package-installation)
    - [Mac OS X Package Installation](#mac-os-x-package-installation)
    - [Install net_guard](#install-net_guard)
  - [Configuring net_guard](#configuring-net_guard)
    - [Maintaining a whitelist](#maintaining-a-whitelist)
    - [Cron job](#cron-job)
    - [Telegram Usage](#telegram-usage)
  - [Usage](#usage)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Net Guard

A command line tool to detect possible malicious activity in your network using ARP protocol. Maybe someone is hacking in your network! Alerts are sent via telegram, remotely add mac address to a whitelist. 

## OS compability
* Linux
* Windows WSL?  (not tested)
* OS X

These are just bash scripts with a call out to cross platform tools like git, wget, arp-scan and curl. 

## Features

* Detection of unknown devices
* Detection of devices in promiscuous mode (approach taken from [here](http://www.securityfriday.com/promiscuous_detection_01.pdf))
* Send a message to a telegram channel about the new network device(s).
* Store Whitelist (allow MACs) in a textfile on a remote git server.  (For remote editing)

## Install 

You will need to have `arp-scan`, `git` and `curl` installed. 

There is a script to do everything in this repo for linux.  [install_net_guard.sh](./install_net_guard.sh)

### Linux and WSL Package Installation
1. `sudo apt-get update && sudo apt-get install -y curl arp-scan git `

### Mac OS X Package Installation
1. `brew install arp-scan`


### Install net_guard
You can paste the contents below for easy install

```shell
sudo curl -o /etc/telegram-notify.conf https://raw.githubusercontent.com/NicolasBernaerts/debian-scripts/master/telegram/telegram-notify.conf
sudo curl -o /usr/local/sbin/telegram-notify https://raw.githubusercontent.com/NicolasBernaerts/debian-scripts/master/telegram/telegram-notify
sudo curl -o /usr/local/sbin/net_guard.sh https://gitlab.com/blockops/net-guard/-/raw/master/net_guard.sh
sudo chmod +x /usr/local/sbin/telegram-notify
sudo chmod +x /usr/local/sbin/net_guard.sh
if [[ ! -f /etc/net_guard.conf ]]; then
  sudo curl -o /etc/net_guard.conf https://gitlab.com/blockops/net-guard/-/raw/master/net_guard.conf
fi
sudo chmod 600 /etc/net_guard.conf
echo "Please update the settings /etc/net_guard.conf"
```

## Configuring net_guard
To configure net_guard, edit the /etc/net_guard.conf file.  If you don't have this file you need to following the instructions steps from the install steps.

At a bare minimum you need to change the following variables to have net_guard work:

* TELEGRAM_CHAT_ID
* TELEGRAM_BOT_TOKEN
* REMOTE_URL_AUTH
* REMOTE_CLONE_URL
* WL_FILE

See the [telegram configuration](#telegram-usage) section for how to setup telegram notifications. 

The configuration files are stored at:

* /etc/net_guard.conf (global conf file)
* ~/.net_guard.conf (local conf file) -- overwrites global variables when run

### Maintaining a whitelist
A whitelist is used to keep track of the mac addresses that are allowed on your network.  This whitelist will be
empty at first until you specify all the mac addresses.

Add the Mac addresses of devices you know to this whitelist file. You can list current connected devices using:

`arp-scan --interface=<xx> --localnet` 

The whitelist file will needs to be remotely stored on a git server (Gitlab preferred) so that net_guard.sh can retrieve the updated list and so you can edit it remotely.  The path to the file will need to be specified in the net_guard.conf file.  Net_guard.sh will also need credentials to retrive the whitelist.  If you are using gitlab you can create a [deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/) which will give you a unique username and password for the script to use. 


In this example below I have a repo named mac_addresses and a whitelist file named `whitelist_home.txt`.

Following these steps to setup your own:

1. Fork this repo: https://gitlab.com/blockops/mac_addresses/-/forks/new
2. Goto your new repository, in the sidebar click settings -> repository
3. Create a [deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/)
4. Copy the username and password from the deploy token into ie. `REMOTE_URL_AUTH="net-guard-user:Nabcdefghunter2Ny"`
5. Copy **your** repository clone url into ie. `REMOTE_CLONE_URL="https://${REMOTE_URL_AUTH}@gitlab.com/blockops/mac_addresses.git"`
    Do not remove the REMOTE_URL_AUTH section. 
6. Add some mac addresses to the whitelist_home.txt file.
7. Goto your repository, in the sidebar click settings -> general -> Project visibility and make it private.

Example repo: https://gitlab.com/blockops/mac_addresses/-/blob/master/whitelist_home.txt

Please remember that your whitelist file in the repository will need to match the name found in the WL_FILE.

Below are the settings pertaining to the steps above.

```
WL_FILE="${TEMP_DIR}/whitelist_home.txt"  (DO NOT REMOVE TEMP_PATH)
REMOTE_URL_AUTH="net-guard-user:Nabcdefghunter2Ny"
REMOTE_CLONE_URL="https://${REMOTE_URL_AUTH}@gitlab.com/blockops/mac_addresses.git"

```

### Cron job
You can create a cron job to run the net_guard.sh script if `RUN_ONCE=1`.
To do this just add the following to your crontab or something similar.  You may need to change the primary network interface to match your system.  On macs it is en0.  On linux it is eth0.  Use `ifconfig` to get a list of interfaces and locate which interface is on the network you wish to scan. 

```
# Cronjob `sudo crontab -e`  # must be run as root
PATH="/bin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin"
# run every minute
0-59 * * * * /usr/local/sbin/net_guard.sh eth0 2&>1 > /var/log/net-guard.log
```

### Telegram Usage
This code is configured to use telegram and will notify a telegram channel with the ip, mac address and manufactor when configured.

You will need to change the /etc/net_guard.conf file to contain valid bot settings. You will see the following in the config file so please ensure you update the TELEGRAM_CHAT_ID and TELEGRAM_BOT_TOKEN configs.

The telegram notifications are made possible via a bash script called [telegram-notify](https://github.com/NicolasBernaerts/debian-scripts/tree/master/telegram) script.  

If you have not created a bot you can easily do so by following the [instructions here](http://www.bernaerts-nicolas.fr/linux/75-debian/351-debian-send-telegram-notification)

```shell
# This is the channel or chat id you wish to dump the message to
# This id can be found by logging into the WEB telgram app (must be web)
# Then goto your group where the message should appear and click in the the address bar
# Yru should see an address with the group or user id
# Example: https://web.telegram.org/#/im?p=g490120355
# The id would be: -490120355
# Please not you must add the negative sign in front
# YOU MUST CHANGE THIS AND REMOVE THE COMMENT 
#TELEGRAM_CHAT_ID="-12323139975"

# You must have a bot created and the BOT Token to send a message to telegram
# http://www.bernaerts-nicolas.fr/linux/75-debian/351-debian-send-telegram-notification
# Go here for instructions
# YOU MUST CHANGE THIS AND REMOVE THE COMMENT 

#TELEGRAM_BOT_TOKEN=""
```


## Usage
`sudo bash ./net_guard.sh <interface>`

`E.g.: ./net_guard.sh en0`

To list all network interfaces:

`ifconfig`

Newly found devices are recorded at `/var/run/new_found.txt`, and are only reported once.

Both whitelist file and `/var/run/new_found.txt` can be edited to include comments, such as:

`00:11:22:33:44:55 # My iPhone`

There are two equivalent lists for promiscuous mode devices (`p_whitelist.txt` and `/var/run/p_new_found.txt`).


## Troubleshooting
You can set the LOG_LEVEL to `debug` for extra output which may help with your issue.

`sudo LOG_LEVEL=debug ./net_guard.sh en0`





