cron{'net_guard':
  ensure => present,
  command => "/usr/local/sbin/net_guard.sh ${networking['primary']} 2&>1 > /var/log/net-guard.log",
  user    => 'root',
  environment => ['PATH="/bin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin"'],
  minute  => "0-59",
}
