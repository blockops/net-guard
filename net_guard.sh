#!/bin/bash

set -e
if [[ -f /etc/net_guard.conf ]]; then source /etc/net_guard.conf; fi
if [[ -f ~/.net_guard.conf ]]; then source  ~/.net_guard.conf; fi

# This was added recently and some users may not have this in their config
NEW_FOUND_EXPIRATION=${NEW_FOUND_EXPIRATION:-'10'}

mkdir -p $STATE_DIR
touch $NF_FILE
touch $P_NF_FILE

INTERFACE=$1
MAC_REGEX='([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}'
IP_REGEX='\b([0-9]{1,3}\.){3}[0-9]{1,3}\b'

if [ ! "$1" ]; then
  echo "ERROR: Missing interface argument."
  echo "E.g. ./net_guard en0"
  exit 1
fi

function update_repo(){
   if [[ -d $TEMP_DIR ]]; then
     log "Updating remote sources"
     git --work-tree=$TEMP_DIR --git-dir=$TEMP_DIR/.git pull --depth=1
   else
    # remove so git can clone it
    log "Getting remote sources"
    rm -rf $TEMP_DIR # since this is a file by default
    mkdir -p $(dirname $TEMP_DIR)
    git clone $REMOTE_CLONE_URL $TEMP_DIR --depth=1 -q
   fi
}

function expire_new_found(){
  # $1 # new found file
  # remove file if the file has expired
  if find $1 -mtime ${NEW_FOUND_EXPIRATION} | grep -q "."; then
    log "${1} has expired, removing file"
    rm -f $1
    touch $1
  fi
}

function should_alert(){
  # $1: mac address found
  # $2: whitelist filename to check against
  # $3: new_found filename to check against

  retval_alert=0
  debug "Checking whitelist $2 for $1"
  if ! grep -q "$1" "$2"
  then
    debug "Checking new_found $3 for $1"
    if ! grep -q "$1" "$3"
    then
      retval_alert=1
    fi
  fi
}


#function alert(){
#  # $1: msg prefix for subject email
#  # $2: mac address found
#  # $3: new_found filename to register

#  msg="\"$1 $2\""
#  echo $msg
#  cat ${CRT_FILE} | mail -s "$msg" ${ALERT_EMAIL_ADD}
#  echo "$2" >> "$3"

#}
function alert(){
  # $1: msg prefix for subject email
  # $2: mac address found
  # $3: ip
  # $4: device_type
  # $5: new_found filename to register

  msg="$1 $2 with ip ${3} of type ${4}"
  echo $msg
  /usr/local/sbin/telegram-notify --user $TELEGRAM_CHAT_ID --key $TELEGRAM_BOT_TOKEN --text "$msg" --document $CRT_FILE
  echo "$2" >> "$5"
}

function bulk_alert(){
  # $1: msg prefix for subject email
  # $2: data 
  log "Bulk telegram message is being sent"
  msg="$2"
  /usr/local/sbin/telegram-notify --error --user $TELEGRAM_CHAT_ID --key $TELEGRAM_BOT_TOKEN  --title "New devices found on the network" --text "$msg" # --document $CRT_FILE
}

function arp_full_scan(){
  log "Running a scan on ${INTERFACE}"
  arp-scan --interface=${INTERFACE} --localnet > ${CRT_FILE}
}

function prom_mode(){
  ips=" "
  cat ${CRT_FILE} | grep -oE ${IP_REGEX} $1 |
  {
    while read ip;
    do 
      ips="$ips $ip"
    done
    arp-scan --interface=${INTERFACE} --destaddr=01:00:5e:00:00:05 $ips | grep -oE ${MAC_REGEX} $1 | while read mac;
    do
      device_ip=$(grep -E $mac $CRT_FILE | cut -f1 -s)
      device_type=$(grep -E $mac $CRT_FILE | cut -f3 -s)
      should_alert $mac ${P_WL_FILE} ${P_NF_FILE}
      if [ "$retval_alert" == 1 ]; then alert "Device on promiscuous mode found:" ${mac} $device_ip $device_type ${P_NF_FILE} ;fi
    done
  }
}
function log(){
  printf '%s %s\n' "[INFO] $(date)" "$1"
}
function debug(){
  if [[ $LOG_LEVEL == 'debug' ]]; then
    printf '[DEBUG] %s %s\n' "$(date)" "$1" 
  fi
}

function new_devices(){
  alerts_file=$(mktemp)
  log "Reading $CRT_FILE"
  cat ${CRT_FILE} | grep -oE ${MAC_REGEX} $1 | while read mac;
  do 
    touch "${TEMP_DIR}/alerts.txt"
    device_ip=$(grep -E $mac $CRT_FILE | cut -f1 -s)
    device_type=$(grep -E $mac $CRT_FILE | cut -f3 -s)
    if [[ ! -z $device_ip ]]; then
      device_name=$(host $device_ip | cut -d\  -f5)
    fi
    should_alert $mac ${WL_FILE} ${NF_FILE}
    if [ "$retval_alert" == 1 ]; then 
     a="$mac # $device_ip $device_name $device_type"
     log "$a"
     echo $mac >> $NF_FILE
     echo "$a" >> $alerts_file 
    fi
  done
  alerts=$(cat $alerts_file)
  if [[ ! -z $alerts ]]; then 
    bulk_alert "New devices found:" "$alerts" $NF_FILE
  fi
  rm -f $alerts_file
}

# ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
  log "Cleaning up..."
  rm -rf $TEMP_DIR
  exit 0
}

while :
do
  log "Press [CTRL+C] to stop.."	
  update_repo
  expire_new_found $NF_FILE
  expire_new_found $P_NF_FILE
  arp_full_scan
  new_devices
#  prom_mode
  rm -rf $TEMP_DIR
  if [[ $RUN_ONCE -eq 1 ]]; then
     ctrl_c 
  fi 
  sleep ${CHECK_INTERVAL}
done


